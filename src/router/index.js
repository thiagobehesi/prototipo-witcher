import Vue from 'vue'
import Router from 'vue-router'
import Inicio from '@/components/inicio'
import itensMaster from '@/components/itens-master'
import Glossario from '@/components/glossario'

import HelloWorld from '@/components/HelloWorld'
import itensView from '@/components/itens-view'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Inicio',
      component: Inicio
    },
    {
      path: '/itens',
      name: 'itensMaster',
      component: itensMaster
    },
    {
      path: '/glossario',
      name: 'glossario',
      component: Glossario
    }
  ]
})
