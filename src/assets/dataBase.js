/*
http://witcher.wikia.com/wiki/Category:The_Witcher_3_steel_weapons
*/
let swords = {
    wolven_steel_sword: { // http://witcher.wikia.com/wiki/Wolven_steel_sword
        name: 'wolven steel sword',
        type: 'Steel sword',
        damage: 0,
        tier: 'witcher',
        wight: 1.71,
        price: null,
        description: '',
        effects: [
            {sing: true, value: 5, target: 'Sign intensity'},
            {sing: true, value: 5, target: 'Adrenaline Point gain'},
            {sing: true, value: 5, target: 'Chance to cause bleeding'},
            {sing: true, value: 5, target: 'Bonus experience from humans and nonhumans'}
        ],
        img: 'wolven_steel_sword.png',
        enhancement: '1 upgrade slot',
        ingredient : [
            { name: 'Leather scraps', amount : 1 },
            { name: 'Monster brain', amount : 1 },
            { name: 'Monster eye', amount : 1 },
            { name: 'Steel ingot', amount : 2 }
        ],
        source: 'craft'
    },
    feline_steel_sword: { // http://witcher.wikia.com/wiki/Feline_steel_sword
        name: 'feline steel sword',
        type: 'Steel sword',
        damage: 0,
        tier: 'magic',
        wight: 3.16,
        price: null,
        description: '',
        effects: [
            {sing: true, value: 5, target: 'Chance to cause bleeding'},
            {sing: true, value: 5, target: 'Bonus experience from humans and nonhumans'}
        ],
        img: 'feline_steel_sword.png',
        enhancement: '1 upgrade slot',
        ingredient : [
            { name: 'Leather scraps', amount : 1 },
            { name: 'Iron ingot', amount : 4 },
            { name: 'Ruby dust', amount : 1 },
            { name: 'Monster saliva', amount : 1 },
        ],
        source: 'craft'
    },
    addan_deith_silver_sword: { // http://witcher.wikia.com/wiki/Addan_Deith#The_Witcher_3:_wild_Hunt
        name: 'addan deith',
        type: 'Silver sword',
        damage: '95-105 + 10 per level',
        tier: 'relic',
        wight: 1.18,
        price: null,
        description: '',
        effects: [
            {sing: true, value: 10, target: 'Aard Sign intensity'},
            {sing: true, value: 10, target: 'Axii Sign intensity'},
            {sing: true, value: 25, target: 'Critical hit damage bonus'},
            {sing: true, value: 4, target: 'Critical hit chance'},
            {sing: true, value: 1, target: 'Chance to freeze'}
        ],
        img: 'addan_deith.png',
        enhancement: '3 upgrade slots',
        ingredient : [],
        source: 'loot'//linkar para personagem
    },
    dislair_silver_sword: { // http://witcher.wikia.com/wiki/Disglair#Disglair_.28crafted.29
        name: 'Disglair',
        type: 'Silver sword',
        damage: '90-95 + 10 per level',
        tier: 'master',
        wight: 1.53,
        price: null,
        description: '',
        effects: [],
        img: 'dislair_silver_sword.png',
        enhancement: null,
        ingredient : [
            { name: 'Leather straps', amount : 1 },
            { name: 'silver ingot', amount : 4 }
        ],
        source: 'Craft'
    }
}