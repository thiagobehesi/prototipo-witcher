export default {
    data () {
        return {
            bestiario: [
                {   //Bear
                    id:'bear',
                    name:'Bear',
                    classe:'Beasts',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/0/07/Tw3_journal_bear.png/revision/latest?cb=20160304204324',
                    citacao:`Know that ditty about the bear "climbing the mountain, to see what he could see?" Biggest load of rubbish I've ever heard. When a bear climbs a mountain, it's not to see. It's to hunt. To kill.
                    — Jahne Oldak, royal huntsman
                    `,
                    descricao:`Bears are omnivores - meaning men find a place in their diet beside berries, roots and salmon. When they snack on humans, they most frequently partake of the meat of travelers unwittingly trespassing on their territory, or else that of hunters for whom besting such a creature is a lifelong ambition.
                    There are several subspecies of bears - black bears, polar bears and cave bears - which differ from one another in coloring as well as in size and strength. All share one trait in common, however: a near-unmatched ability to kill.`,
                    vuneravel: [
                        {id:'beast-oil',name:'Beast oil',img:'https://vignette.wikia.nocookie.net/witcher/images/9/9c/Tw3_oil_beast.png/revision/latest?cb=20170425024457'},
                        {id:'quen',name:'Quen',img:'https://vignette.wikia.nocookie.net/witcher/images/5/5b/Tw3_ability_quen_sign.png/revision/latest?cb=20170317171849'}
                    ]
                },{ //ulfhedinn
                    id:'ulfhedinn',
                    name:'Ulfhedinn',
                    classe:'Cursed One',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/0/06/Tw3_journal_ulfhedinn.png/revision/latest?cb=20160408154719',
                    citacao:`Ulfhedinn? What's that, some kind of fish?

                    - Professor Artibus Joannes Rack, geographer, disappeared during a surveying expedition in Skellige`,
                    descricao:`Ulfhedinn are a breed of werewolf found mainly in Skellige. The harsh and barren conditions of the isles might explain why they primarily hunt men and are stronger than their continental brethren. Older and particularly dangerous ulfhedinn are called olrefs. Only a few daring warriors in Skellige history have managed to defeat an ulfhedinn, and each of them is commemorated in ballads as a hero to this day.

                    Like werewolves, ulfhedinn and vorefs are active at night, particularly when the moon is at its fullest. Fast, strong, and amazingly resilient, these creatures kill with disturbing ease. Silver blades should be brought against them, as should <a>Devil's Puffball</a>. Take note that when near death the ulfhedinn becomes particularly dangerous and will attack with doubled fury, while calling on wolves to come to its rescue.`,
                    vuneravel: [
                        {id:'moon-dust',name:'Moon Dust',img:'https://vignette.wikia.nocookie.net/witcher/images/f/f3/Tw3_bomb_moon_dust.png/revision/latest?cb=20160408155422'},
                        {id:'devil-puffball',name:`Devil's Puffball`,img:'https://vignette.wikia.nocookie.net/witcher/images/f/f3/Tw3_bomb_moon_dust.png/revision/latest?cb=20160408155422'},
                    ]
                },{ //basilisk
                    id:'basilisk',
                    name:'Basilisk',
                    classe:'Draconid',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/b/bc/Tw3_journal_basilisk.png/revision/latest?cb=20160408164921',
                    citacao:`In memory of the noble knight, Roderick, slain during a valiant struggle against a basilisk. Let's hope the beast choked on his bones.
                    — Gravestone inscription, Vizima cemetery.`,
                    descricao:`Contrary to popular belief, basilisks cannot turn anything to stone with their gaze. That is small comfort, however, given that their acid, venom, claws and teeth provide them many other ways to kill.
                    Basilisks love dark, damp places such as cellars, caves and city sewers. They hunt by day, waiting patiently in hiding for their prey to come, then jump out in a flash to unleash a deadly attack.
                    When preparing to fight such a creature one should drink Golden Oriole, which will provide resistance to its venom, and also prepare Dancing Star or shrapnel bombs, which work particularly well against basilisks.
                    Basilisk leather is a highly-valued material used to make fashionable shoes and women's handbags. For this reason many men, their courage girded by goldlust, take to hunting them. Most of these hunts end in disaster, but some do manage to bag their prey, which has led to a drastic decline in this creature's numbers in recent years. Some mages and druids are of the opinion that basilisks should be included in programs meant to safeguard dying species. Everyone else thinks those mages and druids have gone completely mad.`,
                    vuneravel: [
                        {id:'golden-oriole',name:'Golden oriole',img:'https://vignette.wikia.nocookie.net/witcher/images/0/04/Tw3_potion_golden_oriole.png/revision/latest?cb=20160430083318'},
                        {id:'grapeshot',name:'Grapeshot',img:'https://vignette.wikia.nocookie.net/witcher/images/7/79/Tw3_bomb_grapeshot.png/revision/latest?cb=20160529190740'}
                    ]
                },{ //Ice Elemental
                    id:'ice-elemental',
                    name:'Ice Elemental',
                    classe:'elemental',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/c/c6/Tw3_journal_iceelemental.png/revision/latest?cb=20160409141539',
                    citacao:`I thought to myself - what's a hunk of ice doing in the middle of some lab? And then that hunk of ice got up and broke my legs.
                    – Yannick Lovt, burglar`,
                    descricao:`An ice elemental is a mass of frozen water animated by magic. Deprived of consciousness or independent will, this elemental is boundlessly obedient to the orders of the mage who created it. Those orders usually contain but one syllable: kill.`,
                    vuneravel: [
                        {id:'dimeritium-bomb',name:'Dimeritium bomb',img:'https://vignette.wikia.nocookie.net/witcher/images/5/5b/Tw3_bomb_dimeritium.png/revision/latest?cb=20160409173449'},
                        {id:'elementa-oil',name:'Elementa oil',img:'https://vignette.wikia.nocookie.net/witcher/images/0/02/Tw3_oil_elementa.png/revision/latest?cb=20170425025417'}
                    ]
                },{ //Ghouls
                    id:'ghouls',
                    name:'Ghouls',
                    classe:'Necrophage',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/c/c9/Tw3_journal_ghoul.png/revision/latest?cb=20160502084919',
                    citacao:`Ghouls creep and crawl at night
                    Eating everything in sight
                    In a snap they'd eat you, too'
                    Chop you up for a ghoulish stew!
                    — Children's rhyme`,
                    descricao:`Ghouls and graveirs are hard to describe. In part, they resemble humans – yet on the whole, they are the utter negation of all that is human. Though they have arms and legs like men, they walk on all fours like dogs or badgers. Though they have eerily familiar faces, one searches them in vain for any sign of sentiment, reason or even a spark of consciousness. They are driven by one thing and one thing only: an insatiable craving for human flesh.`,
                    vuneravel: [
                        {id:'necrophage-oil',name:'Necrophage oil',img:'https://vignette.wikia.nocookie.net/witcher/images/7/73/Tw3_oil_necrophage.png/revision/latest?cb=20170425025716'}
                    ]
                }
            ],
            personagens: [
                {
                    id:'yennefer',
                    name:'Yennefer',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/4/47/Tw3_journal_yennefer.png/revision/latest?cb=20160328143927',
                    descricao:`The witcher first met the raven-haired sorceress a good twenty years back. Their friendship and feelings between them were born of a common adventure involving a genie and a wish granted to Geralt that intertwined their fates inextricably.
                    In the time since then their relationship had, however, been quite stormy - rich in ups and downs, crises and break-ups. Geralt and Yennefer's love provides irrefutable proof that "opposites attract."
                    A few years ago Geralt and Yennefer had, after a long separation full of adventures for them both, gotten back together again. Their moment of repose was interrupted by the Wild Hunt, which took Yennefer captive. The witcher set out at once to save her, but lost his memory while doing so. When he finally recovered it, he immediately set off once more on his quest to find his beloved sorceress.
                    The circumstances of Geralt's initial reunion with Yennefer after two years were quite different than he had imagined. The sorceress was not only safe and sound, but had even secured the aid of an unexpected and mighty ally - the Nilfgaardian Empire.`,
                },
                {
                    id:'ciri',
                    name:'Ciri',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/f/f5/Tw3_journal_ciri.png/revision/latest?cb=20160527150710',
                    descricao:`
                        Cirilla Fiona Elen Riannon - what can I possibly say about her? That we call her Ciri for short, that she was born in 1251, that she has ashen hair and a scar on her cheek?
                        All true, and that's the Cirilla I know best, one I first laid eyes on those many years ago, the one who seemed thoroughly, well, not ordinary, but certainly not as extraordinary as she in fact is.
                        For Cirilla is also a highly-skilled witcher, heiress to several thrones, the last bearer of the Elder Blood, a powerful Source endowed with exceptional magic talent and the Lady of Time and Space. Her hair colour and date of birth seem... rather incidental now, don't they?
                        I could also tell you she is Geralt's adopted daughter - but that would be a gross simplification. Ciri is much more. She is his Destiny, his Unexpected Child, someone bound to the witcher by Fate's most inextricably tangled fetters.
                        Following age-old witcher tradition, Geralt took Ciri to Kaer Morhen when she came into his care. There he and Vesemir taught her in the ways of the professional monster slayer. It was then that her magic talents were first revealed, and they discovered she was a Source.
                        Ciri's gift proved a curse as well. Because of it, she would one day have to hide from the entire world- even Geralt.
                        Ciri's biography contained one more great secret. Her natural father was none other than the emperor of Nilfgaard, Emhyr var Emreis. His words confirmed the fears swirling in Geralt's mind. Ciri had returned and was in mortal danger, for the unrelenting Wild Hunt was on her trail.
                        Yennefer made it clear why the Wild Hunt wanted Ciri: Eredin wanted the power latent in her Elder Blood. She also let Geralt know that Ciri had been seen in war-ravageed Velen as well as in Novigrad, the largest city in the world.
                        Reports that Ciri had spent time at Crow's Perch proved true. She was there as the guest of the local warlord, Philip Strenger, also known as the Bloody Baron. Despite his violent moniker, this man treated Ciri with kindness and respect.
                        It seemed that during Ciri's time in Velen she got into a quarrel with some sort of witch or witches in the swamps.
                        Geralt learned the truth of Ciri's time in the swamps from the mouths of the hideous Crones themselves. Even Ciri, better able to hold her own than most anyone in existence, was lucky to escape from these powerful beings alive and intact.
                        The visions revealed by the Mask of Uroboros made it clear beyond all doubt: Ciri had played a role in the magic catastrophe on Ard Skellig. More importantly, they confirmed she had been in Skellige - and then fled in the face of grave danger.
                        Ultimate, tangible proof of the truth that the emperor had spoken the truth about Ciri's pursuers came in the form of a corpse - one belonging to a warrior of the Wild Hunt. The Riders truly were on Ciri's trail.
                        In hindsight, putting Ciri in touch with Whoreson was not one of my brightest ideas. In my defense I can only say that her situation was so perilous even the riskiest plan seemed better than inactivity.
                        After my adventures and mishaps, Geralt finally found Ciri on the Isle of Mists. When he crossed the threshold into the room in which she slumbered, the protective spell Avallac'h had cast upon her snapped under the sheer weight of their combined destinies. Geralt was reunited with his adopted daughter after years of separation and searching. No words can describe the joy he felt in that moment.`,
                },
                {
                    id:'keira',
                    name:'Keira Metz',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/6/6f/Tw3_journal_keira.png/revision/latest?cb=20160327100213',
                    descricao:`Geralt first met Keira Metz when she literally dropped on top of him out of nowhere. During the infamous coup on Thanedd Island, Keira was defenestrated and nearly landed right on the witcher's head. The next time Geralt saw her, in the swamps of Velen, was shocking in a different way - the luxury-loving sorceress was the last person he expected to see in such a grim and barren place.
                    Geralt later learned the reason for this, and Keira's fate gave him ample fodder for contemplating the cruel whimsies of the wheel of fortune. Once the esteemed advisor to the now late King Foltest, she had been chased out of Temeria when she lost that ruler's trust. Later she joined the Lodge of Sorceresses, which earned her the hatred of Redania's king and Nilfgaard's emperor alike. Because of this, she had gone deep undercover, posing as a cunning woman, a village witch of sorts, deep in the Velen boondocks. It was not at all difficult to tell that she despised every minute of this.
                    Keira then asked Geralt for another favor: lifting the curse from the tower of Fyke Isle. Curses and favors being what Geralt does best, he obliged.`,
                },
                {
                    id:'sigismund',
                    name:'Sigismund Dijkstra',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/c/c7/Tw3_journal_dijkstra.png/revision/latest?cb=20170315181519',
                    descricao:`The individual masquerading as Sigi Reuven was in fact none other than Sigismund Dijkstra, the former head of Redanian intelligence and a man Geralt and I had had many dealings with in the past.
                    He had fallen out of King Radovid's favor and nothing had been heard of him for many years. Now it seemed he had finally decided to emerge from the shadows, but instead of returning to high political wrangling he dove deep into the criminal underground – and quickly surfaced as one of its leading figures.
                    Though he did not show it, in his own way Dijkstra respected Geralt – even though the very thought of their last meeting brougt a pained grimace to his face. The two had found themselves standing in each other's way during the coup on Thanedd Island. The stalemate was quickly broken when Geralt summarily broke Dijkstra's leg.
                    The spy's life story would make for a postively enthralling adventure tale. A victim of Philippa Eilhart's intrigues, he had been forced to flee Redania at breakneck speed – or have his own neck broken by assassins. For a certain time he sought refuge in far-off lands, but in the end he decided to return to the Free City of Novigrad.`,
                },
                {
                    id:'eredin',
                    name:'Eredin Bréacc Glas',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/4/49/Tw3_journal_eredin.png/revision/latest?cb=20160508060422',
                    descricao:`The name of the King of the Wild Hunt, the identity of the lord of nightmares, the being behind the frightful mask - this long remained unknown. Over time, however, scraps of information gradually coalesced into a full likeness of our foe - but did nothing to detract from the terror he inspired.
                    The Wild Hunt was in truth an elite cavalry brigade from the world of the Aen Elle, the Alder Folk, and was commanded by their ambitious and ruthless king, Eredin Breacc Glas. He would travel via secret paths through the cold emptiness between his world from ours to capture victims and take them back to his homeland as slaves.
                    The current object of his Hunt was Cirilla, whose power he wanted to harness for his own uses. The only obstacle in his path - Geralt of Rivia.
                    Ciri was able to shed a bit of light on the commander of the Wild Hunt's motivations. The threat of annihilation hung over the Aen Elle homeland. Eredin, a warrior and a conqueror by nature, decided to solve this problem in the simplest possible way - by seizing our world. The key to doing so? Ciri's power, which would allow him to open the gates to a full-scale invasion.`,
                },
                {
                    id:'dandelion',
                    name:'Dandelion',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/a/a6/Tw3_journal_dandelion.png/revision/latest?cb=20160411194949',
                    descricao:`I would wager anyone that you, dear reader, are a person of culture and taste - and therefore already familiar with me, Dandelion, and the role I am to play in the following tale. Nevertheless, allow me to sketch a few lines by way of self-portrait, for the sake of thoroughness, and in the event you have spent much of the last half-century in some dark corner where the light of my star has yet to reach.
                    "Born in 1229, a talented poet and troubadour, a graduate of Oxenfurt Academy, a frequent performer at royal courts, an unequaled lover appreciated, and in some cases adored, by ladies worldwide, a skilled negotiator and a stirring orator" - such is the image of the bard Dandelion as painted by his friends and promoters.
                    This image is, of course, somewhat overbright in its coloring - I personally prefer to think of myself as a dedicated artist in thrall to his Muse, one whose work has benefited immeasurably from the fact that I was, am and forever will remain a close friend and steadfast companion to the witcher Geralt. It is his fate I chronicle in this present work and his story which I shall sing till the end of my days.
                    Having learned of my disappearance, Geralt dropped everything to find out what had become of me. Though at first he suspected the cause of my trouble lay in my many and turbulent affairs of the heart, he later determined that I had strayed into a life of crime, seeking to steal treasure belonging to one of the leaders of Novigrad's underworld.
                    The motivations for my actions became clear some time later. I proved that, when it came to helping Cirilla, I would not balk at sticking my head into the lion's maw, let alone crossing such common scum as Cyprian Wiley, better known (for good reason!) as Whoreson.
                    Only a madman calls it courage to fight when faced with overwhelming odds. Though known for my capricious and unpredictable charm, I am not yet considered mad, and so, while defending my companion's escape to the very last, I ultimately allowed the temple guard to take me to their prison, where I awaited my imminent execution.`,
                },
                {
                    id:'radovid',
                    name:'Radovid V',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/a/ad/Tw3_journal_radovid.png/revision/latest?cb=20150906175743',
                    descricao:`It did not take long for King Radovid of Redania to prove himself a hard and ruthless ruler, one fully deserving to be styled "the Stern." His father, King Vizimir, was murdered when Radovid was quite young, and his mother, Hedwig of Malleore, and a Regency Council ruled in his stead.
                    Young Radovid soon seized power in his own right, however, and wrought vengeance on those who had treated him with disrespect. He took to forcing all his potential political opponents to swear allegiance – or face death.
                    He waged war not only against Nilfgaard, but also against mages, whom he saw as the root of all evil. Radovid also made every effort to gain control over the Free City of Novigrad, whose fleet and treasury could tip the scales of the ongoing conflict towards Radovid's victory.
                    Geralt's meeting with Radovid confirmed the rumors circling around the king's mental state. The Redanian king was a dangerous madman trapped in his own world of disturbing visions. This did not make him one jot less intelligent or cunning, however.`,
                },
                {
                    id:'vesemir',
                    name:'Vesemir',
                    img:'https://vignette.wikia.nocookie.net/witcher/images/9/92/Tw3_journal_vesemir.png/revision/latest?cb=20170315191944',
                    descricao:`Vesemir was the oldest living member of the Wolf School and most likely the oldest witcher of any school on the Continent.
                    About as long in years as the ruins of Kaer Morhen themselves and eternally complaining about his creaky bones, this master of the witcher trade gave no thought to a well-deserved retirement. Gray, but still spry, he continued to ply the monster hunting trade into his golden years – effectively, too, as he'd seen more beasts than all his students put together.
                    A harsh and demanding instructor in Geralt's youth, over the years he had become something of an adoptive father and mentor to the other witchers, always ready to help with sage advice and steady hands.
                    In the spring of 1272, when our story begins, Vesemir had joined Geralt on his search for Yennefer, trekking with him through war-ravaged Temeria.
                    Vesemir always said no witcher had ever died in his own bed, so death in combat surely awaited him as well. Death's waiting ended on the mournful day when the Wild Hunt descended on Kaer Morhen in pursuit of Ciri. Vesemir gave his all to protect his former ward, whom he had always treated like an adopted granddaughter, and died a hero's death at the hands of Imlerith, the Hunt's cruel general.`,
                },
            ]
        }
    }
}